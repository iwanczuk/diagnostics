<?php

namespace Magento\Diagnostics\Ui\Component\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;

class Reporting extends \Magento\Framework\View\Element\UiComponent\DataProvider\Reporting
{
    /**
     * @param CollectionFactory $collectionFactory
     * @param FilterPool $filterPool
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        FilterPool $filterPool
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->filterPool = $filterPool;
    }
}
