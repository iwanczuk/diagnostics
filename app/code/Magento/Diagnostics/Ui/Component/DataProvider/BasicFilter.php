<?php

namespace Magento\Diagnostics\Ui\Component\DataProvider;

use Magento\Framework\Data\Collection;
use Magento\Framework\Api\Filter;

class BasicFilter
{
    /**
     * Apply regular filters like collection filters
     *
     * @param AbstractDb $collection
     * @param Filter $filter
     * @return void
     */
    public function apply(Collection $collection, Filter $filter)
    {
        $collection->addFilter($filter->getField(), $filter->getValue());
    }
}
