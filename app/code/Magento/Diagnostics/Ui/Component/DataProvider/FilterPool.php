<?php

namespace Magento\Diagnostics\Ui\Component\DataProvider;

use Magento\Diagnostics\Ui\Component\DataProvider\BasicFilter;
use Magento\Framework\Data\Collection;
use Magento\Framework\Api\Search\SearchCriteriaInterface;

class FilterPool
{
    /**
     * @var array
     */
    protected $applier;

    /**
     * @param array $appliers
     */
    public function __construct(BasicFilter $applier)
    {
        $this->applier = $applier;
    }

    /**
     * @param AbstractDb $collection
     * @param SearchCriteriaInterface $criteria
     * @return void
     */
    public function applyFilters(Collection $collection, SearchCriteriaInterface $criteria)
    {
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $this->applier->apply($collection, $filter);
            }
        }
    }
}
