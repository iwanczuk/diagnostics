# Diagnostics

A Magento CE 2.0+ module allowing to preview database processes, system processes on unix-like platform and internal cron schedule.

## Installation Instructions

1. Backup your store database and web directory.
2. Using your favorite FTP client upload module files to your store root.
3. Run "php bin/magento module:enable Magento_Diagnostics" in your store root.
4. Run "php bin/magento setup:upgrade" in your store root.
