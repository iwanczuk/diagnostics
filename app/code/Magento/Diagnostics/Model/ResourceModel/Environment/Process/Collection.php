<?php

namespace Magento\Diagnostics\Model\ResourceModel\Environment\Process;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;

class Collection extends \Magento\Framework\Data\Collection implements SearchResultInterface
{
    /**
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * @param EntityFactoryInterface $entityFactory
     */
    public function __construct(EntityFactoryInterface $entityFactory)
    {
        parent::__construct($entityFactory);
        $this->setItemObjectClass('Magento\Framework\View\Element\UiComponent\DataProvider\Document');
    }

    /**
     * Load data
     *
     * @param bool $printQuery
     * @param bool $logQuery
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function loadData($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded()) {
            return $this;
        }

        if (!function_exists('shell_exec') || strtoupper(substr(php_uname(), 0, 3)) === 'WIN') {
            $this->_setIsLoaded();

            return $this;
        }

        $processes = $this->applyFilters(
            $this->parseProcesses(
                $this->fetchProcesses()
            )
        );

        $this->_totalRecords = count($processes);
        $this->_setIsLoaded();

        $offset = ($this->getCurPage() - 1) * $this->getPageSize();
        $count = $this->getPageSize();

        foreach (array_slice($processes, $offset, $count) as $process) {
            $item = $this->getNewEmptyItem();
            $item->addData($process);
            $this->addItem($item);
        }

        return $this;
    }

    /**
     * @return string
     */
    private function fetchProcesses()
    {
        $sort = [];
        foreach ($this->_orders as $field => $dir) {
            $sort[] = ($dir == self::SORT_ORDER_DESC ? '-' : '+') . $field;
        }
        $sortStr = implode(',', $sort);
        if ($sortStr) {
            $sortStr = '--sort ' . $sortStr;
        }
        return shell_exec('ps -e --format pid=,uname=,group=,pcpu=,pmem=,args= ' . $sortStr);
    }

    /**
     * @param string $input
     * @return array
     */
    private function parseProcesses($input)
    {
        preg_match_all('/
            (?P<pid>[0-9]+)[[:blank:]]+
            (?P<uname>[0-9a-z]+)[[:blank:]]+
            (?P<group>[0-9a-z]+)[[:blank:]]+
            (?P<pcpu>[0-9]+\.[0-9]+)[[:blank:]]+
            (?P<pmem>[0-9]+\.[0-9]+)[[:blank:]]+
            (?P<args>.+)
        /ix', $input, $matches, PREG_SET_ORDER);
        $processes = [];
        $keys = array_flip(['pid', 'uname', 'group', 'pcpu', 'pmem', 'args']);
        foreach ($matches as $match) {
            $processes[] = array_intersect_key($match, $keys);
        }
        return $processes;
    }

    /**
     * @param array $processes
     * @return array
     */
    private function applyFilters(array $processes)
    {
        return array_filter($processes, function ($process) {
            foreach ($this->_filters as $filter) {
                if (false === strpos($process[$filter['field']], $filter['value'])) {
                    return false;
                }
            }
            return true;
        });
    }

    /**
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @param AggregationInterface $aggregations
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }


    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        return [];
    }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        return null;
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }

    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setItems(array $items = null)
    {
        return $this;
    }
}
